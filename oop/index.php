<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");

echo "Nama hewan :". " " .  $sheep->name;// "shaun"
echo "<br>"; 
echo "Legs : " . " " . $sheep->legs . " legs"; // 2
echo "<br>";
echo " Cold Blooded : " . " " .  $sheep->cold_blooded; // false
echo "<br> <br>";

$sungokong = new Ape("kera sakti");

echo "Nama hewan :" . " " . $sungokong->name;
echo "<br>";
echo " Legs : " . " " . $sungokong->legs . " legs";
echo "<br>";
echo " Cold Blooded : " . " " .  $sungokong->cold_blooded; 
echo "<br>";
$sungokong->yell(); // "Auooo"

echo "<br> <br>";

$kodok = new Frog("buduk");

echo "Nama hewan :" . " " . $kodok->name;
echo "<br>";
$kodok->legs = 4;
echo " Legs : " . " " . $kodok->legs . " legs";
echo "<br>";
echo " Cold Blooded : " . " " .  $kodok->cold_blooded; 
echo "<br>";
$kodok->jump(); // "Auooo"
