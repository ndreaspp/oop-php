<?php 

class Frog {
    public $name, $legs = 2, $cold_blooded = "false";
    
    public function __construct($name) {
    $this->name = $name;
    
}

    public function jump() {
        echo "hop hop";
}
}